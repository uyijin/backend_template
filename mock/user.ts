// test.ts
import type { MockMethod } from "vite-plugin-mock";
import type { ResponseData } from "../src/typings";

export default [
  {
    url: "/user/login",
    method: "post",
    response: (): ResponseData => {
      return {
        result: true,
        data: {
          token: "test",
        },
      };
    },
  },
  {
    url: "/user/refresh",
    method: "post",
    response: (): ResponseData => {
      return {
        result: true,
        data: {
          token: "test",
        },
      };
    },
  },
] as MockMethod[];
