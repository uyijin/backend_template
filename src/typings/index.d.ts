export interface User {
  id: number;
  name: string;
  token: string;
}

export interface Menu extends MenuItem {
  icon: string;
  children: null | MenuItem[];
}
export interface MenuItem {
  name: string;
  router: string;
}
export interface ResponseData {
  result: boolean;
  message?: string;
  data?: any;
}
