import { createRouter, createWebHashHistory } from "vue-router";
import LoginPage from "../views/LoginPage.vue";
import PageContainer from "../views/PageContainer.vue";

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: LoginPage,
    },
    {
      path: "/page",
      name: "Page",
      component: PageContainer,
      beforeEnter: (to, from) => {
        localStorage.setItem("prevPath", from.path);
        localStorage.setItem("currentPath", to.path);
      },
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: () =>
            import(
              /* webpackChunkName: "dashboard" */ "../views/pages/DashboardIndex.vue"
            ),
        },
      ],
    },
  ],
});

export default router;
