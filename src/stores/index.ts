import { defineStore } from "pinia";
import type { User } from "@/typings";

export const useStore = defineStore("main", {
  state: (): { user: User; systemName: string } => {
    return {
      user: {
        id: 0,
        name: "",
        token: "",
      },
      systemName: "宣传教育",
    };
  },
  actions: {
    afterLogin(user: User) {
      this.user = user;
      localStorage.setItem("auth", user.token);
    },
    beforeLogout() {
      this.user = {
        id: 0,
        name: "",
        token: "",
      };
      localStorage.removeItem("auth");
    },
  },
});
