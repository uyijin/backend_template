import axios from "axios";
import router from "@/router";
import { ElMessage } from "element-plus";
import { useStore } from "@/stores";

const config = {
  baseURL: import.meta.env.VITE_API_DOMAIN || "",
  timeout: 60 * 1000, // Timeout
};

const _axios = axios.create(config);

_axios.interceptors.request.use(
  function (config) {
    // console.log(config);

    const store = useStore();
    // 添加授权token
    const newHeader = {
      Authorization: store.user.token,
    };
    return {
      ...config,
      headers: config.headers
        ? {
            ...config.headers,
            ...newHeader,
          }
        : newHeader,
    };
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    if (response.data.result) {
      return Promise.resolve(response.data.data);
    } else {
      ElMessage.error({
        message: response.data.message || response.data.err,
        showClose: true,
        duration: 10000,
      });
      return Promise.reject(response.data);
    }
  },
  function (error) {
    const store = useStore();
    if (error.response && error.response.status === 401) {
      store.beforeLogout();
      router.push("/").catch((e) => console.log(e));
    } else {
      ElMessage.error({
        message: "网络错误",
        showClose: true,
        duration: 3000,
      });
    }
    // Do something with response error
    return Promise.reject(error);
  }
);

export default _axios;
